-- Is Client?
local _, IsClient = pcall(function() return game.Players.LocalPlayer end)

local pcall = pcall
local type = type
local tostring = tostring
local typeof = typeof
local error = error
local getmetatable = getmetatable
local setmetatable = setmetatable
local newproxy = newproxy

local TrapMeta = setmetatable({},{__metatable = getmetatable(game)})
local TrapTable = setmetatable({},{__index = function() end, __newindex = function() end, __eq = function() end, __metatable = getmetatable(game)})
local InternalKey = "c6$aj(;</2oB,By&o.1,RjCD/AQlORSANDBOX"

local Service = setmetatable({},{__index = function(self, index) return game:GetService(index) end, __metatable = getmetatable(game)})

local ChatService = Service.Chat

-- Fall-back Environment
local Environment = getfenv(0)

-- Metatable Locks
local MetatableLockKey = "c6$aj(;</2oB,By&o.1,RjCD/AQlORLOCKMETA_NOGGR"

-- Includes
local Sessions = require(script.Parent.Sessions)
local Network

-- Internal Functions
local function Gen(User, Type, ...)
	local Session = Sessions:GetSession()
	Session:AddThread()
    local args = {...}
    for i = 1, select("#",...) do
        args[i] = coroutine.wrap(setfenv(function()
			Session:AddThread()
            return tostring(args[i])
        end,Session.Environment))()
    end
	if not Network then
		Network = shared("Td6xcvcCy%15j'4.wvC5tG{Y3fS7oP^AmS[6He)k31^1~8C56#y_y2U0d`Qe7`}","Get",{Request = "NetworkModule"})
	end

    Network:FireOutput(User, Type, ((table.concat(args,"\t") or nil)))
end

-- Internal Tables

--- Sandbox table
local Sandbox = {}

local GlobalBox = {
	-- Values
}

local Replacements = {
	
}

local Objects = {
	Fake = setmetatable({},{__mode = "kv"}), -- Real Object -> Fake Object
	Real = setmetatable({},{__mode = "kv"}), -- Fake Object -> Real Object
	Strings = {}                             -- Already stored string objects
}

local DestroyProtectedObjects = {
	["Player"] = true
}

local NonReplicatingObjects = { -- If object is true, it will not replicate
	["ScreenGui"] = true,
	["Frame"] = true
}

local NonReplicatingProperties = {
	["Text"] = true
}

-- Internal Functions
local function addFakeObject(RealObject, FakeObject)
	Objects.Fake[RealObject] = FakeObject
end

local function addRealObject(FakeObject, RealObject)
	Objects.Real[FakeObject] = RealObject
end

local function getRealObject(FakeObject)
	return Objects.Real[FakeObject] or FakeObject
end

local function getRealObjects(...)
	local Data = { ... }
	local Parse = {  }	
	
	for i,v in next, Data do
		table.insert(Parse, getRealObject(v))
	end
	return unpack(Parse)
end

local function getFakeObject(RealObject)
	return Objects.Fake[RealObject] or RealObject
end

local function finialiseSandboxObject(FakeObject, RealObject)
	addFakeObject(RealObject, FakeObject)
	addRealObject(FakeObject, RealObject)
end

local function FilterString(input, player)
	local alreadyParsed = Objects.Strings[input]
	if alreadyParsed then
		return alreadyParsed
	end
	
	local newString = ChatService:FilterStringForBroadcast(input, player)
	Objects.Strings[input] = newString	
	
	return newString
end

local function addSandboxGlobal(Name, Value)
	for newName in Name:gmatch("%a+") do
		GlobalBox[newName] = Value
	end
end

local function isObjectSandboxObj(Object)
	local _, IsSandboxObject = pcall(function() return Object()._METAKEY == MetatableLockKey end)
	return ((typeof(Object) == "userdata") and (IsSandboxObject)) or (typeof(Object) ~= "userdata")
end

local function createReplacementObject(ClassName)
	local Methods = {}
	local Properties = {}
	if Replacements[ClassName] then
		return Replacements[ClassName]
	end
	
	local newTable = {
		inheritors = {},		
	}	
	
	Replacements[ClassName] = setmetatable(newTable,{
		__call = function(self, index)
			return { Methods, Properties }
		end
	})	
	
	function newTable:inherit(className)
		self.inheritors[#self.inheritors] = Replacements[className]
	end
	
	function newTable:addMethod(name, method)
		if self ~= newTable then
			return error("expected ':' but used with '.'")
		end
		
		for newName in name:gmatch("%a+") do
			Methods[newName] = function(self, ...) 
				if isObjectSandboxObj(self) then
					return method(getRealObject(self), ...)
				end
			end
		end
		return newTable
	end
	
	function newTable:addProperty(name, get, set)
		if self ~= newTable then
			return error("expected ':' but used with '.'")
		end
		
		for newName in name:gmatch("%a+") do
			local Get, Set
			
			if get then
				Get = function(self, ...)
					if isObjectSandboxObj(self) then
						return get(getRealObject(self), ...)
					end
				end
			end
			if set then
				Set = function(self, ...)
					if isObjectSandboxObj(self) then
						return set(getRealObject(self), ...)
					end
				end
			end
			
			Properties[newName] = {
				["Get"] = Get,
				["Set"] = Set
			}
		end
		return newTable
	end
	
	function newTable:getMethod(name)
		if Methods[name] then
			return Methods[name]
		end
		for i,v in next, self.inheritors do
			if v()[name] then
				return v()[name]
			end
		end
		return nil
	end
		
	return newTable
end

local function getMember(Object, Index)
	return Object[Index]
end

local function setMember(Object, Index, Value)
	Object[Index] = Value
end

local Replacement = createReplacementObject

-- Sandbox functions
function Sandbox:SandboxObject(Object)
	local Type = typeof(Object)
	local Session = Sessions:GetSession()
	local Player
	if Session ~= nil then
		if Session["Player"] then
			Player = Session["Player"]
		end
	end
	
	local fakeObject = Objects.Fake[Object]
	
	if fakeObject then
		return fakeObject
	end
	
	if Type == "table" then
		if getmetatable(Object) == nil then
			for i,v in next, Object do
				Object[i] = Sandbox:SandboxObject(v)			
			end
		end
		return Object
	elseif Type == "function" then
		return function(...)
			Sessions:GetSession():AddThread()
			return Sandbox:SandboxObject(Object(getRealObjects(...)))
		end
	elseif Type == "string" then
		if Player then
			return FilterString(Object, Player)
		else
			return Object
		end
	elseif Type == "Instance" or Type == "RBXScriptSignal" then
		local _, IsSandboxObject = pcall(function() return Object()._METAKEY == MetatableLockKey end)
		if IsSandboxObject then
			local newData = Sandbox:SandboxUserdata(Object) -- SANDBOX USERDATA
			finialiseSandboxObject(newData, Object)
			return newData
		end
	end
	
	return Object
end

function Sandbox:SandboxObjects(...)
	local Data = { ... }
	local Parse = {  }	
	
	for i,v in next, Data do
		table.insert(Parse, Sandbox:SandboxObject(v))
	end
	return unpack(Parse)
end

function Sandbox:SandboxUserdata(Object)
	local Type = typeof(Object)
	local newObject = newproxy(true)	
	
	if Type == "Instance" then
		local newMeta = getmetatable(newObject)
		local toString = tostring(Object)
		local className = Object.ClassName
		
		newMeta.__metatable = setfenv(function(key)
			if key ~= MetatableLockKey then
				return error("attempt to call a userdata value", 0)
			else
				return {_METAKEY = MetatableLockKey}
			end
		end,{})
		
		function newMeta:__tostring()
			return toString		
		end
		
		function newMeta:__index(index)
			local response, result = pcall(getMember, Object, index)

			if not response then
				return error(result, 0)
			end			
			
			local Call, Output, replacement
			local descriptor = Replacements[className] or Replacements["General"]
			if descriptor then
				if typeof(result) == "function" then
					replacement = descriptor:getMethod(index)
				end
			end
			
			if typeof(result) == "function" then
				if not replacement then
					Call, Output = true, setfenv(Sandbox:SandboxObject(result), Sessions:GetSession().Environment)
				else
					Call, Output = true, setfenv(replacement, Sessions:GetSession().Environment) 
				end
				
				if not Call then
					return error(Output:match("%S+:%d+: (.*)$") or Output, 0)
				end
				
				if type(Output) ~= "function" then
	                return Sandbox:SandboxObject(Output)
	            else
	                return Output
	            end
			elseif result then
				return Sandbox:SandboxObject(result)	
			else
				return error(index.." is not a valid member of "..className,0)			
			end
		end
	elseif Type == "RBXScriptSignal" then
		local newMeta = getmetatable(newObject)
		local toString = tostring(Object)
		local internalTable = setmetatable({},{__metatable = getmetatable(Object)})		
		
		function newMeta:__tostring()
			return toString
		end
		
		function internalTable:wait()
            return Sandbox:SandboxObject(Object.wait(getRealObject(self)))
		end
		
		function internalTable:connect(Function)
	        if typeof(Function) ~= "function" then
	            return error("Attempt to connect failed: Passed value is not a function", 0)
	        end
			local Session = Sessions:GetSession()
			local Return, Connection
	
	        Connection = Object.connect(getRealObject(self), function(...)
				Session:AddThread()
	            
	            local Success, Result = pcall(setfenv(function(...)
					Session:AddThread()
					--[[if not Session.Alive() then
						Connection:disconnect()
						return error("Script Ended", 0)
					end]]--
	
	                setfenv(Function,Session.Environment)(Sandbox:SandboxObjects(...))
	            end,Session.Environment), ...)
				
				if not Success then
					pcall(function() Connection:disconnect() end)
					spawn(function() error(Result, 0) end)
	
					return warn("Disconnected event because of exception")
				end
	        end)
	        
	        local fakeConnection = newproxy(true)
	        local fakeConnectionMeta = getmetatable(fakeConnection)
	        local fakeConData = setmetatable({},{__metatable = "This metatable is locked"})
	        
	        function fakeConData:disconnect()
	            pcall(function() Connection:disconnect() end)
	            fakeConData.connected = false
	        end
	        
	        function fakeConnectionMeta:__index(Index)
	            if fakeConData[Index] then
	                return fakeConData[Index]
	            else
	                return Sandbox:SandboxObject(Connection[Index]) or nil
	            end
	        end
	        
	        function fakeConnectionMeta:__tostring()
	            return "Connection"
	        end
	        
	        fakeConnectionMeta.__metatable = getmetatable(Connection)
	        
	    	return fakeConnection
		end
		
		function newMeta:__index(Index)
            if internalTable[Index] then
                return internalTable[Index]
            else
                return Sandbox:SandboxObject(Object[Index])
            end
        end
            
        function newMeta:__newindex(Item)
            return error(("%s cannot be assigned to"):format(Item),0)
        end
        
        newMeta.__metatable = getmetatable(Object)
        
		for i,v in next, internalTable do
			if type(v) == "function" then
				internalTable[i] = setfenv(v, Sessions:GetSession().Environment)
			end
		end
	end
	
	return newObject
end

--[[ Replacements ]]--

Replacement("General") -- THIS IS AN EXCEPTION! ACTS FOR ALL OBJECTS
:addMethod("Remove,remove", function(self)
	if typeof(self) == "Instance" and DestroyProtectedObjects[self.ClassName] then
		return error'Cannot use remove on a this object!'
	else
		return game.Remove(self)
	end
end)
:addMethod("Destroy,destroy", function(self)
	if typeof(self) == "Instance" and DestroyProtectedObjects[self.ClassName] then
		error'Cannot use destroy on a this object!'
	else
		return game.Destroy(self)
	end
end)

Replacement("Player")
:addMethod("Kick,kick", function(self)
	if typeof(self) == "Instance" and DestroyProtectedObjects[self.ClassName] then
		error'Cannot kick players!'
	else
		return game.Players:GetPlayers()[1].Kick(self)
	end
end)

local function Tuple(...)
	return {n=select("#",...),...}
end

local SharedObjects = {
	["coroutine"] = {
		create = function(f)
			local th = coroutine.create(f)
			Sessions:GetSession():AddThread(th)
			return th
		end,
		resume = coroutine.resume;
		wrap = function(f)
			local th = coroutine.create(f)
			Sessions:GetSession():AddThread(th)
			return setfenv(function(...)
				local res = Tuple(coroutine.resume(th,...))
				if res[1] then return unpack(res,2,res.n) end
				error(res[2],2)
			end,getfenv(0))
		end,
		running = coroutine.running,
		status = coroutine.status,
		yield = function() return game:GetService("RunService").Stepped:wait() end,
	},
	["spawn"] = function(f)
		local session = Sessions:GetSession()
		spawn(function(...)
			session:AddThread()
			f(...)
		end)
	end,
	["Spawn"] = function(f)
		local session = Sessions:GetSession()
		spawn(function(...)
			session:AddThread()
			f(...)
		end)
	end,
	["delay"] = function(t,f)
		local session = Sessions:GetSession()
		delay(t,function(...)
			session:AddThread()
			f(...)
		end)
	end,
	["Delay"] = function(t,f)
		local session = Sessions:GetSession()
		delay(t,function(...)
			session:AddThread()
			f(...)
		end)
	end,
	["pcall"] = function(f,...)
		local session = Sessions:GetSession()
		return pcall(function(...)
			session:AddThread()
			return f(...)
		end,...)
	end,
	["ypcall"] = function(f,...)
		local session = Sessions:GetSession()
		return pcall(function(...)
			session:AddThread()
			return f(...)
		end,...)
	end,
    ["xpcall"] = function(f, c)
		local session = Sessions:GetSession()
        return xpcall(function(...) 
            session:AddThread()
            f(...)
        end, function(...)
            session:AddThread()
            c(...)
        end)
    end,
}

--[[ Globals ]]--
addSandboxGlobal("game,Game", Sandbox:SandboxObject(game))
addSandboxGlobal("workspace,Workspace", Sandbox:SandboxObject(workspace))
addSandboxGlobal("_G", setmetatable({},{__metatable = getmetatable(game)}))
addSandboxGlobal("shared", setmetatable({},{__metatable = getmetatable(game)}))
addSandboxGlobal("print", function(...)
        Gen(getRealObject(Sessions:GetSession().Owner), 1, ...)
    end)
addSandboxGlobal("warn", function(...)
        Gen(getRealObject(Sessions:GetSession().Owner), 3, ...)
    end)

for i,v in next, SharedObjects do
	addSandboxGlobal(i, v)
end

local SBs = {}

function Sandbox:CreateEnvironment(Table, KillFunction, Owner, Script, IsIrc, Channel)
	local Store = {}
	
	local Session = Sessions:NewSession({Environment = Table, Owner = Owner})
   	Session:AddThread()	
	
	return setmetatable(Table, {
       	__index = function(self, index)
			local GlobalVal = GlobalBox[index]
			local Original = Environment[index]
			if Store[index] == "nil" then
				return nil
			end
			if not GlobalVal then
				if not Store[index] then
					if Original then
						return Original or GlobalBox["_G"][index]
					end
				end
			else
				Store[index] = GlobalVal
			end
			return Store[index]
		end,
		
		__newindex = function(self, index, value)
          	if value == nil then
            	Store[index] = "nil"
          	else
            	rawset(Table, index, value)
          	end
		end,
		
		__mode = "kv",
		
		__metatable = getmetatable(game)
	}), Session
end

local function CheckKey(Key)
    if Key == InternalKey then
        return true
    else
        return false
    end
end

local function ModuleReturn(Key)
    local Sandbox = Sandbox
    local Check, Response = pcall(CheckKey, Key)

    if not Check then
        return TrapTable
    end

    if Response then
        return Sandbox
    end

    return TrapTable
end

return setfenv(ModuleReturn, TrapMeta)